package Domain;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@javax.persistence.Entity
@Table(name = "phone")
public class PhoneNumber extends Entity{


	private String countyPrefix;
	private String cityPrefix;
	private String number;
	private EnumerationValue type;

	public String getCountyPrefix() {
		return countyPrefix;
	}

	public void setCountyPrefix(String countyPrefix) {
		this.countyPrefix = countyPrefix;
	}

	public String getCityPrefix() {
		return cityPrefix;
	}

	public void setCityPrefix(String cityPrefix) {
		this.cityPrefix = cityPrefix;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	public EnumerationValue getType() {
		return type;
	}

	public void setType(EnumerationValue type) {
		this.type = type;
	}

}
