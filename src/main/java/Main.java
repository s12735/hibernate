import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import Cache.Cache;
import Cache.Updater;
import Domain.Adress;
import Domain.Entity;
import Domain.Person;
import Domain.PhoneNumber;
import Domain.User;
import Repository.Imp.RepositoryCatalog;
import UnitOfWork.IUnitOfWork;
import UnitOfWork.UnitOfWork;
import checker.rules.PasswordRule;

public class Main {

	private static SessionFactory buildSessionFactory() {
		try {
			return new Configuration().configure().buildSessionFactory();
		} catch (Throwable ex) {

			System.err.println("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static void main(String[] args) throws Exception {

		SessionFactory session = buildSessionFactory();
		session.openSession();
		session.getCurrentSession().beginTransaction();

		PhoneNumber phoneNumber = new PhoneNumber();
		phoneNumber.setNumber("586740968");

		Adress address = new Adress();
		address.setCity("Władysławowo");
		address.setStreet("Dettlaffa");

		Person person = new Person();
		person.setFirstName("Łukasz");
		person.setPhoneNumber(phoneNumber);
		person.setAddress(address);

		User luki = new User();
		luki.setLogin("luki");
		luki.setPassword("lacky11");
		luki.setPerson(person);

		IUnitOfWork uow = new UnitOfWork(session);

		PasswordRule rule = new PasswordRule();
		RepositoryCatalog catalog = new RepositoryCatalog(session, uow);
		catalog.getUserChecker().addRule(rule);

		catalog.getUsers().save(luki);
		catalog.commit();

		// updater
		Updater updater = new Updater(catalog);
		updater.setLifespan(1);
		updater.update(catalog);
		

		@SuppressWarnings("unchecked")
		List<Entity> usersFromDb = (List<Entity>) Cache.getInstance().getItems().get("Users");

		for (Entity dbUser : usersFromDb)
			System.out.println(dbUser.getId() + " " + ((User) dbUser).getLogin() + " " + ((User) dbUser).getPassword());

		// updater
		updater.run();

		for (Entity dbUser : usersFromDb)
			System.out.println(dbUser.getId() + " " + ((User) dbUser).getLogin() + " " + ((User) dbUser).getPassword());
	}

}