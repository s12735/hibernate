package Repository;

import java.util.List;
import Domain.Entity;

public interface IRepository<TEntity> {
	public void save(TEntity entity) throws Exception;
	public void update(TEntity entity) throws Exception;
	public void delete(TEntity entity);
	public TEntity get(int id);
	public List<Entity> getAll();
}
