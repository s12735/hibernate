package Repository;

import Domain.Person;
import Domain.Role;
import Domain.User;

public interface IRepositoryCatalog {

	public IRepository<User> getUsers();
	public IRepository<Person> getPersons();
	public IRepository<Role> getRoles();
	public void commit();
}