package Repository.Imp;

import java.util.List;

import org.hibernate.SessionFactory;

import Domain.Entity;
import Domain.Person;
import Repository.IEntityBuilder;
import UnitOfWork.IUnitOfWork;
import checker.RuleChecker;

public class PersonRepository 
extends Repository<Person>{

protected PersonRepository(SessionFactory session,
		IEntityBuilder<Person> builder, IUnitOfWork uow, RuleChecker<Person> checker) {
	super(session, builder, uow, checker);
}

public Person get(int id) {
	return session.getCurrentSession().get(Person.class, id);
}

@SuppressWarnings("unchecked")
public List<Entity> getAll() {
	session.getCurrentSession().beginTransaction();
	return session.getCurrentSession().createQuery("SELECT u FROM Person u")
			.list();
}


}