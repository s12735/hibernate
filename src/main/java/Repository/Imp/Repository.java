package Repository.Imp;

import org.hibernate.SessionFactory;

import Domain.Entity;
import Repository.IEntityBuilder;
import Repository.IRepository;
import UnitOfWork.IUnitOfWork;
import UnitOfWork.IUnitOfWorkRepository;
import checker.CheckResult;
import checker.RuleChecker;
import checker.RuleResult;

public abstract class Repository<TEntity extends Entity> implements IRepository<TEntity>, IUnitOfWorkRepository {
	protected RuleChecker<TEntity> checker;
	protected IUnitOfWork uow;
	protected SessionFactory session;
	protected IEntityBuilder<TEntity> builder;

	protected Repository(SessionFactory session, IEntityBuilder<TEntity> builder, IUnitOfWork uow,
			RuleChecker<TEntity> checker) {
		this.uow = uow;
		this.builder = builder;
		this.session = session;
		this.checker = checker;

	}

	private CheckResult checkRules(TEntity entity) {

		for (CheckResult c : checker.check(entity)) {
			if (c.getResult() == RuleResult.Error) {
				return c;
			}
		}
		return null;
	}

	public void save(TEntity entity) throws Exception {
		CheckResult result = checkRules(entity);

		if (result == null)
			uow.markAsNew(entity, this);
		else
			throw new Exception(result.getMessage());
	}

	public void update(TEntity entity) throws Exception {
		CheckResult result = checkRules(entity);

		if (result == null)
			uow.markAsDirty(entity, this);
		else
			throw new Exception(result.getMessage());
	}

	public void delete(TEntity entity) {
		uow.markAsDeleted(entity, this);
	}

	public void persistAdd(Entity entity) {

		session.getCurrentSession().save(entity);
	}

	public void persistUpdate(Entity entity) {

		session.getCurrentSession().update(entity);
	}

	public void persistDelete(Entity entity) {

		session.getCurrentSession().delete(entity);
	}

}
