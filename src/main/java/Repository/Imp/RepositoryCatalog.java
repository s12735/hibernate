package Repository.Imp;


import org.hibernate.SessionFactory;

import Domain.Person;
import Domain.Role;
import Domain.User;
import Repository.IRepository;
import Repository.IRepositoryCatalog;
import Repository.IUserRepository;
import UnitOfWork.IUnitOfWork;
import checker.RuleChecker;

public class RepositoryCatalog implements IRepositoryCatalog{

	private SessionFactory session;
	private IUnitOfWork uow;
	private RuleChecker<User> userChecker = new RuleChecker<User>();
	private RuleChecker<Person> personChecker = new RuleChecker<Person>();
	
	public RuleChecker<User> getUserChecker() {
		return userChecker;
	}

	public RuleChecker<Person> getPersonChecker() {
		return personChecker;
	}
	
	public RepositoryCatalog(SessionFactory session, IUnitOfWork uow) {
		super();
		this.session = session;
		this.uow = uow;
	}

	public IUserRepository getUsers() {
		
		return new UserRepository(session, new UserBuilder(), uow, userChecker);
	}


	public IRepository<Role> getRoles() {
		return null;
	}

	public void commit() {
		uow.commit();
	}

	public IRepository<Person> getPersons() {
		return new PersonRepository(session, new PersonBuilder(), uow, personChecker); 
	}

}
