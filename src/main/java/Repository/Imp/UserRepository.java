package Repository.Imp;

import java.util.List;

import org.hibernate.SessionFactory;

import Domain.Entity;
import Domain.Role;
import Domain.User;
import Repository.IEntityBuilder;
import Repository.IUserRepository;
import UnitOfWork.IUnitOfWork;
import checker.RuleChecker;

public class UserRepository 
extends Repository<User> implements IUserRepository{

	public UserRepository(SessionFactory session, IEntityBuilder<User> builder, IUnitOfWork uow, RuleChecker<User> checker) {
		super(session,builder, uow, checker);
	}

	

	
	public List<User> withRole(Role role) {
		return null;
	}

	
	public List<User> withRole(String roleName) {
		return null;
	}

	
	public List<User> withRole(int roleId) {
		return null;
	}



	public User get(int id) {
		return session.getCurrentSession().get(User.class, id);
	}


	@SuppressWarnings("unchecked")
	public List<Entity> getAll() {
		session.getCurrentSession().beginTransaction();
		return session.getCurrentSession().createQuery("SELECT u FROM User u")
				.list();
	}

	
}
