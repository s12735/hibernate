package checker;

public interface ICheckRule<TEntity> {
	public CheckResult checkRule(TEntity entity);
}
