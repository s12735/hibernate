package checker;

import java.util.ArrayList;
import java.util.List;

public class RuleChecker<TEntity> {

	private List<ICheckRule<TEntity>> rules = new ArrayList<ICheckRule<TEntity>>();
	
	public void addRule(ICheckRule<TEntity> rule){
		rules.add(rule);	
	}
	
	public List<CheckResult> check(TEntity entity){
		List<CheckResult> result = new ArrayList<CheckResult>();
		for(ICheckRule<TEntity> rule: rules){
			result.add(rule.checkRule(entity));
		}
		return result;
	}
}
