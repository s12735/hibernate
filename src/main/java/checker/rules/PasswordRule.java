package checker.rules;

import Domain.User;
import checker.CheckResult;
import checker.ICheckRule;
import checker.RuleResult;

public class PasswordRule implements ICheckRule<User>{

	public CheckResult checkRule(User entity) {

	if(entity.getPassword()==null)
		return new CheckResult("", RuleResult.Error);
	if(entity.getPassword().equals(entity.getLogin()))
		return new CheckResult("", RuleResult.Error);
	if(entity.getPassword().length()<8)
		return new CheckResult("Password too short.", RuleResult.Error);
	
	return new CheckResult("", RuleResult.Ok);
}
}
