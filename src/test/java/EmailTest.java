

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import Domain.Person;
import checker.CheckResult;
import checker.RuleResult;
import checker.rules.EmailRule;

public class EmailTest {
	EmailRule rule = new EmailRule();
	
	@Test
	public void check_email_should_return_false(){
		Person p = new Person();
		p.setEmail(".dadads@w.p");
		CheckResult result = rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Error));
	}
	
	
	@Test
	public void check_email_should_return_true(){
		Person p = new Person();
		p.setEmail("s111111111@pjwstk.edu.pl");
		CheckResult result = rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Ok));
	}
}

