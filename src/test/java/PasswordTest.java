

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import Domain.User;
import checker.CheckResult;
import checker.RuleResult;
import checker.rules.PasswordRule;

public class PasswordTest {

	PasswordRule rule = new PasswordRule();

	@Test
	public void check_password() {
		User u = new User();
		u.setLogin("s12735");
		u.setPassword("dsdsdds");
		CheckResult result = rule.checkRule(u);
		assertTrue(result.getResult().equals(RuleResult.Error));
	}
	
	
	@Test
	public void check_password_should_return_true() {
		User u = new User();
		u.setLogin("s12735");
		u.setPassword("aS912sdrq");					// pass heve to contains, one lowercase characters
													// one uppercase characters and one digit from 0-9, min. 8 char.
		CheckResult result = rule.checkRule(u);
		assertTrue(result.getResult().equals(RuleResult.Ok));
	}
}
